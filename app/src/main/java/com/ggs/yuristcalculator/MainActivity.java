package com.ggs.yuristcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.ggs.yuristcalculator.gosposh.Gosposhlina;
import com.ggs.yuristcalculator.kapRemont.KapitalnyReomt;
import com.ggs.yuristcalculator.kommunal.Kommunalnie_uslugi;
import com.ggs.yuristcalculator.kontrakt.ProsrochkaKontrakta;
import com.ggs.yuristcalculator.pozakonupotrebitel.PravaPotrebiteley;

import soup.neumorphism.NeumorphCardView;

public class MainActivity extends AppCompatActivity {
NeumorphCardView neo, kapRem, gosposhlina, ispKontrakta, potrebit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        neo=findViewById(R.id.gosUsl);
        kapRem =findViewById(R.id.kapRem);
        gosposhlina =findViewById(R.id.dosP);
        ispKontrakta =findViewById(R.id.ispKontrakta);
        potrebit =findViewById(R.id.potrebit);





        neo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gos = new Intent(MainActivity.this, Kommunalnie_uslugi.class);
                startActivity(gos);
            }
        });

        kapRem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kr = new Intent(MainActivity.this, KapitalnyReomt.class);
                startActivity(kr);
            }
        });

        gosposhlina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gp = new Intent(MainActivity.this, Gosposhlina.class);
                startActivity(gp);
            }
        });

        ispKontrakta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gp = new Intent(MainActivity.this, ProsrochkaKontrakta.class);
                startActivity(gp);
            }
        });

        potrebit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pp = new Intent(MainActivity.this, PravaPotrebiteley.class);
                startActivity(pp);
            }
        });

    }
}