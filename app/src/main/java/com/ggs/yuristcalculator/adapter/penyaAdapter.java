package com.ggs.yuristcalculator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ggs.yuristcalculator.Model.penyaModel;
import com.ggs.yuristcalculator.R;

import java.util.List;

public class penyaAdapter extends RecyclerView.Adapter<penyaAdapter.ViewHolder> {

    Context context;
    List <penyaModel> penya_list;

    public penyaAdapter(Context context, List<penyaModel> penya_list) {
        this.context = context;
        this.penya_list = penya_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);



        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        if (penya_list != null && penya_list.size()>0){
            penyaModel model = penya_list.get(position);
            holder.tab_zad.setText(model.getZadoljnost());
            holder.tab_stavka.setText(model.getStavka());
            holder.tab_dney.setText(model.getDnej());
            holder.tab_dolya.setText(model.getDolya());
            holder.tab_peni.setText(model.getPeni());

        }
        else
        {
            return;
        }

    }

    @Override
    public int getItemCount() {
        return penya_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

       TextView tab_zad, tab_dney, tab_stavka, tab_dolya, tab_peni;
        public ViewHolder(@NonNull View itemView) {
                        super(itemView);


                        tab_zad = itemView.findViewById(R.id.tab_zad);
            tab_dney = itemView.findViewById(R.id.tab_dney);
            tab_stavka = itemView.findViewById(R.id.tab_stavka);
            tab_dolya = itemView.findViewById(R.id.tab_dolya);
            tab_peni = itemView.findViewById(R.id.tab_peni);
        }
    }
}
