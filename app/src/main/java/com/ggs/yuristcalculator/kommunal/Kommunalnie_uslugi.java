package com.ggs.yuristcalculator.kommunal;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ggs.yuristcalculator.Model.penyaModel;
import com.ggs.yuristcalculator.NeuroToast;
import com.ggs.yuristcalculator.R;
import com.ggs.yuristcalculator.adapter.penyaAdapter;
import com.sdsmdg.tastytoast.TastyToast;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import ir.androidexception.datatable.DataTable;
import ir.androidexception.datatable.model.DataTableHeader;
import ir.androidexception.datatable.model.DataTableRow;
import soup.neumorphism.NeumorphButton;
import soup.neumorphism.NeumorphCardView;

import static java.security.AccessController.getContext;

public class Kommunalnie_uslugi extends AppCompatActivity{
NeumorphButton text;
TextView tost_tv, test2;
RecyclerView recyclerView;
penyaAdapter adapter;
NeumorphCardView pressed_card;
Double stavka;
    int year_2, month2, day2;
    ImageView imageViewCalenderFirst;
    ImageView imageViewCalenderSecond;
    EditText editTextBirthDay;
    EditText editTextBirthMonth;
    EditText editTextBirthYear;
    EditText editTextCurrentDay;
    EditText editTextCurrentMonth;
    EditText editTextCurrentYear;
    EditText stavkaRef;
    EditText summaProsrochki;
    DataTableRow row, row2, row3;


    CheckBox checkBox;
    private static SimpleDateFormat simpleDateFormat0 = new SimpleDateFormat("dd.MM.yyyy");
    public double dney =0.0;
    public double budni =0.0;
    public double dolg = 0;
    String total2;
    DataTable dataTable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kommunalnie_uslugi);
        tost_tv = findViewById(R.id.toast);
        DataTable dataTable = findViewById(R.id.data_table);
        text=findViewById(R.id.button);

//        recyclerView = findViewById(R.id.recycler);

       // setRecycler();


        imageViewCalenderFirst=findViewById(R.id.imageViewCalenderFirst);
     imageViewCalenderSecond=findViewById(R.id.imageCalendarPogash);


        editTextBirthDay=findViewById(R.id.dayStart);
        editTextBirthMonth=findViewById(R.id.monthStart);
        editTextBirthYear=findViewById(R.id.yearStart);


        editTextCurrentDay=findViewById(R.id.dayPogash);
        editTextCurrentMonth=findViewById(R.id.monthPogash);
        editTextCurrentYear=findViewById(R.id.yearPogash);


        stavkaRef=findViewById(R.id.stavkaRef);
        summaProsrochki=findViewById(R.id.summaProsrochki);
        checkBox=findViewById(R.id.checkBox);

test2 = findViewById(R.id.test2);
        pressed_card=findViewById(R.id.pressed_card);


        final Calendar c = Calendar.getInstance();
        editTextCurrentYear.setText(String.valueOf(c.get(Calendar.YEAR)));
        editTextCurrentMonth.setText(addZero(c.get(Calendar.MONTH) + 1));
        editTextCurrentDay.setText(addZero(c.get(Calendar.DAY_OF_MONTH)));

        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
        Date date = new Date(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) - 1);
        String dayOfWeek = simpledateformat.format(date);

//-------------------------------------------------rabotayu s datoy





       imageViewCalenderFirst.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               final Calendar c = Calendar.getInstance();
               int mYear = c.get(Calendar.YEAR);
               int mMonth = c.get(Calendar.MONTH);
               int mDay = c.get(Calendar.DAY_OF_MONTH);

               DatePickerDialog datePickerDialog = new DatePickerDialog(Kommunalnie_uslugi.this, new DatePickerDialog.OnDateSetListener() {
                   @Override
                   public void onDateSet(DatePicker view, int year,
                                         int monthOfYear, int dayOfMonth) {

                       editTextBirthDay.setText(addZero(dayOfMonth));
                       editTextBirthMonth.setText(addZero(monthOfYear + 1));
                       editTextBirthYear.setText(String.valueOf(year));
                   }
               }, mYear, mMonth, mDay);
               datePickerDialog.show();
           }
       });


        imageViewCalenderSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Kommunalnie_uslugi.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        editTextCurrentDay.setText(addZero(dayOfMonth));
                        editTextCurrentMonth.setText(addZero(monthOfYear + 1));
                        editTextCurrentYear.setText(String.valueOf(year));
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        NeumorphButton but2 = findViewById(R.id.button2);
but2.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        editTextBirthDay.setText("");
        editTextBirthMonth.setText("");
        editTextBirthYear.setText("");

        editTextCurrentDay.setText("");
        editTextCurrentMonth.setText("");
        editTextCurrentYear.setText("");
        checkBox.setChecked(false);
        pressed_card.setVisibility(View.GONE);
    }
});

















        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                if (!TextUtils.isEmpty(editTextBirthDay.getText()) && !TextUtils.isEmpty(editTextBirthMonth.getText()) && !TextUtils.isEmpty(editTextBirthYear.getText()) && !TextUtils.isEmpty(summaProsrochki.getText())) {
                    calculateAge();
                    pressed_card.setVisibility(View.VISIBLE);
                    if (checkBox.isChecked()){

                        dney++;
                    }


                    if (dney==0){
                        dataTable.setVisibility(View.GONE);
                    }





//--------------------------------zapolnenie tablici
                    DataTableHeader header = new DataTableHeader.Builder()
                            .item("Задолженность", 2)
                            .item("Дней", 1)
                            .item("Ставка", 1)
                            .item("Доля ставки", 1)
                            .item("Пени", 1)

                            .build();

                    ArrayList<DataTableRow> rows = new ArrayList<>();
                    // define 200 fake rows for table


                    if (dney>= 1 && dney<=30){

                        DataTableRow row = new DataTableRow.Builder()
                                .value(total2)
                                .value(String.valueOf("30"))
                                .value(String.valueOf(stavka).concat(" %"))
                                .value(String.valueOf("0"))
                                .value(String.valueOf("0"))

                                .build();
                        rows.add(row);
                        dataTable.setVisibility(View.VISIBLE);
                        dney=dney-30;

                    }

                       else if (91>dney && dney >30) {


                        DataTableRow row = new DataTableRow.Builder()
                                .value(total2)
                                .value(String.valueOf("30"))
                                .value(String.valueOf(stavka).concat(" %"))
                                .value(String.valueOf("0"))
                                .value(String.valueOf("0"))

                                .build();
                        rows.add(row);
                        dataTable.setVisibility(View.VISIBLE);

                        dney = dney - 30;



                        String formattedDouble3 = String.format("%.2f", dolg*dney/300*stavka/100);

                        DataTableRow row2 = new DataTableRow.Builder()
                                .value(total2)
                                .value(String.valueOf((int)dney))
                                .value(String.valueOf(stavka).concat(" %"))
                                .value(String.valueOf("1/300"))
                                .value(String.valueOf(formattedDouble3).concat(" p."))

                                .build();


                        double penn;
                        penn = dolg + (dolg*dney/300*stavka/100);
                        String formattedItogo1 = String.format("%.2f", dolg*dney/300*stavka/100);
                        String formattedpen = String.format("%.2f", penn);

                        DataTableRow row55 = new DataTableRow.Builder()
                                .value("")
                                .value(String.valueOf(""))
                                .value(String.valueOf(""))
                                .value(String.valueOf("Итого: "))
                                .value(String.valueOf(formattedItogo1).concat(" p."))
                                .build();
                        DataTableRow row56 = new DataTableRow.Builder()
                                .value("")
                                .value(String.valueOf(""))
                                .value(String.valueOf(""))
                                .value(String.valueOf("Общий долг: "))
                                .value(String.valueOf(formattedpen).concat(" pуб."))
                                .build();
                        rows.add(row2);
                        rows.add(row55);
                        rows.add(row56);
                    }



             else if (dney>90){



                                dney = dney-60-30;
              //                  Toast.makeText(Kommunalnie_uslugi.this, dolg*dney/130*stavka/100+" penya", Toast.LENGTH_SHORT).show();

                                double h = dney;
                                double r= dolg*26.0/130.0*stavka/100;
                                String l = String.valueOf(r);
                 //       test2.setText(l +" "+ dolg + "     дней    "+ dney +" " + stavka+"            БУДНИ         "+budni);

                        DataTableRow rower = new DataTableRow.Builder()
                                .value(total2)
                                .value(String.valueOf("30"))
                                .value(String.valueOf(stavka).concat(" %"))
                                .value(String.valueOf("0"))
                                .value(String.valueOf("0").concat(" p."))

                                .build();

                        String formattedDouble2 = String.format("%.2f", dolg*60.0/300*stavka/100);
                                DataTableRow row3 = new DataTableRow.Builder()
                                        .value(total2)
                                        .value(String.valueOf("60"))
                                        .value(String.valueOf(stavka).concat(" %"))
                                        .value(String.valueOf("1/300"))
                                        .value(String.valueOf(formattedDouble2).concat(" p."))
                                        .build();

                        String formattedDouble = String.format("%.2f", dolg*h/130*stavka/100);
                                DataTableRow row4 = new DataTableRow.Builder()
                                        .value(total2)
                                        .value(String.valueOf((int)dney))
                                        .value(String.valueOf(stavka).concat(" %"))
                                        .value(String.valueOf("1/130"))
                                        .value(String.valueOf(formattedDouble).concat(" p."))
                                        .build();

                double itogo = (dolg*60.0/300*stavka/100)+(dolg*h/130*stavka/100);
                        String formattedItogo = String.format("%.2f", itogo);

                        DataTableRow row5 = new DataTableRow.Builder()
                                .value("")
                                .value(String.valueOf(""))
                                .value(String.valueOf(""))
                                .value(String.valueOf("Итого: "))
                                .value(String.valueOf(formattedItogo).concat(" p."))
                                .build();
                        dataTable.setVisibility(View.VISIBLE);

                                rows.add(rower);
                                rows.add(row3);
                                rows.add(row4);
                                rows.add(row5);

                            }









                    //  dataTable.setVisibility(View.VISIBLE);
                    dataTable.setDividerColor(R.color.primary);
                    dataTable.setHeader(header);
                    dataTable.setRows(rows);
                    dataTable.inflate(getApplicationContext());


//---------------------------------------------button



















                    //     nextBirthday();
                } else {

                    //NeuroToast.showToast(Kommunalnie_uslugi.this);
                    TastyToast.makeText(getApplicationContext(), "Проверьте правильность заполнения!", TastyToast.LENGTH_LONG, TastyToast.INFO);
                }


            }
        });
    }

    private String addZero(int number) {
        String n;
        if (number < 10) {
            n = "0" + number;
        } else {
            n = String.valueOf(number);
        }
        return n;
    }

    private void setRecycler() {

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new penyaAdapter(this, getList());
        recyclerView.setAdapter(adapter);
    }

    private List<penyaModel> getList() {
        List<penyaModel> penyaList = new ArrayList<>();
        penyaList.add(new penyaModel("100", "10", "4.5", "1/300", "500"));


        return penyaList;

    }





    private void calculateAge() {

        String enteredText = String.valueOf(stavkaRef.getText());
        //Заменяем запятую на точку.
        enteredText = enteredText.replaceAll(",", ".");
        stavka = Double.parseDouble(enteredText);





        String enteredText2 = String.valueOf(summaProsrochki.getText());
        //Заменяем запятую на точку.
        enteredText2 = enteredText2.replaceAll(",", ".");
        dolg = Double.parseDouble(enteredText2);


        total2 = String.valueOf(dolg);





        int currentDay = Integer.valueOf(editTextCurrentDay.getText().toString());
            int currentMonth = Integer.valueOf(editTextCurrentMonth.getText().toString());
            int currentYear = Integer.valueOf(editTextCurrentYear.getText().toString());

            Date now = new Date(currentYear, currentMonth, currentDay);

            int birthDay = Integer.valueOf(editTextBirthDay.getText().toString());
            int birthMonth = Integer.valueOf(editTextBirthMonth.getText().toString());
            int birthYear = Integer.valueOf(editTextBirthYear.getText().toString());

            Date dob = new Date(birthYear, birthMonth, birthDay);

            if (dob.after(now)) {
                TastyToast.makeText(getApplicationContext(), "Ошибка! Проверьте даты!", TastyToast.LENGTH_LONG, TastyToast.WARNING);
               // Toast.makeText(this, " Ошибка! Проверьте даты! ", Toast.LENGTH_SHORT).show();
                return;
            }



//        try {
//            //Dates to compare
//            String CurrentDate=  currentMonth+"/"+ currentDay + "/" +currentYear;
//            String FinalDate=   birthMonth+"/"+ birthDay + "/" +birthYear;
//
//            Date date1;
//            Date date2;
//
//            SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");
//
//            //Setting dates
//            date1 = dates.parse(CurrentDate);
//            date2 = dates.parse(FinalDate);
//
//            //Comparing dates
//            long difference = Math.abs(date1.getTime() - date2.getTime());
//            long differenceDates = difference / (24 * 60 * 60 * 1000);
//            //    long diffInYears = ChronoUnit.YEARS.between((Temporal) date1, (Temporal) date2);
//            //      long diffInWeeks = ChronoUnit.WEEKS.between(date1.toInstant(), date2.toInstant());
//            //Convert long to String
//            String dayDifference = Long.toString(differenceDates);
//
//            Toast.makeText(this, "Второй подсчет, дней =  "+dayDifference, Toast.LENGTH_SHORT).show();
//            Log.e("HERE","HERE: " + dayDifference);
//
//        } catch (Exception exception) {
//            Log.e("DIDN'T WORK", "exception " + exception);
//        }











            // days of every month


            int month[] = {31, 28, 31, 30, 31, 30, 31,
                    31, 30, 31, 30, 31};

            // if birth date is greater then current birth
            // month then do not count this month and add 30
            // to the date so as to subtract the date and
            // get the remaining days
            if (birthDay > currentDay) {
                currentDay = currentDay + month[birthMonth - 1];
                currentMonth = currentMonth - 1;
            }

            // if birth month exceeds current month, then do
            // not count this year and add 12 to the month so
            // that we can subtract and find out the difference
            if (birthMonth > currentMonth) {
                currentYear = currentYear - 1;
                currentMonth = currentMonth + 12;
            }

            // calculate date, month, year
            int calculated_date = currentDay - birthDay;
            int calculated_month = currentMonth - birthMonth;
            int calculated_year = currentYear - birthYear;

            year_2=calculated_year;
            month2=calculated_month;
            day2=calculated_date;

            TextView test = findViewById(R.id.test);
            test.setText("Неуплата в течении "+year_2+" лет "+month2 + " месяцев " +day2+" дней.");
         long d=   calculateDays(dob, now);
         int dd = (int) d;
        dney=dd+2.0;

         long da = days(dob, now);

            budni=da;


      //  Toast.makeText(this, dney+"         dney", Toast.LENGTH_SHORT).show();





        String FinalDate=  currentDay+"."+ currentMonth + "." +currentYear;
        String CurrentDate=   birthDay+"."+ birthMonth + "." +birthYear;


        long o =
        getDayCount(CurrentDate, FinalDate);
        dney = o;
    //    Toast.makeText(this, "popitka # 3 ="+o, Toast.LENGTH_SHORT).show();



    }


    public static long calculateDays(Date dateEarly, Date dateLater) {
        return (dateLater.getTime() - dateEarly.getTime()) / (24 * 60 * 60 * 1000);
    }


    static long days(Date start, Date end){
        //Ignore argument check

        Calendar c1 = Calendar.getInstance();
        c1.setTime(start);
        int w1 = c1.get(Calendar.DAY_OF_WEEK);
        c1.add(Calendar.DAY_OF_WEEK, -w1);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(end);
        int w2 = c2.get(Calendar.DAY_OF_WEEK);
        c2.add(Calendar.DAY_OF_WEEK, -w2);

        //end Saturday to start Saturday
        long days = (c2.getTimeInMillis()-c1.getTimeInMillis())/(1000*60*60*24);
        long daysWithoutWeekendDays = days-(days*2/7);

        // Adjust days to add on (w2) and days to subtract (w1) so that Saturday
        // and Sunday are not included
        if (w1 == Calendar.SUNDAY && w2 != Calendar.SATURDAY) {
            w1 = Calendar.MONDAY;
        } else if (w1 == Calendar.SATURDAY && w2 != Calendar.SUNDAY) {
            w1 = Calendar.FRIDAY;
        }

        if (w2 == Calendar.SUNDAY) {
            w2 = Calendar.MONDAY;
        } else if (w2 == Calendar.SATURDAY) {
            w2 = Calendar.FRIDAY;
        }

        return daysWithoutWeekendDays-w1+w2;
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }




//--------------------------------------------------------------------NAKONEC-tO PRAVILNO RABOTAYUWIY PODSCHET!!!!!!!!!!!!!!!!!
    public static long getDayCount(String start, String end) {
        long diff = -1;
        try {
            Date dateStart = simpleDateFormat0.parse(start);
            Date dateEnd = simpleDateFormat0.parse(end);

            //time is always 00:00:00 so rounding should help to ignore the missing hour when going from winter to summer time as well as the extra hour in the other direction
            diff = Math.round((dateEnd.getTime() - dateStart.getTime()) / (double) 86400000);
        } catch (Exception e) {
            //handle the exception according to your own situation
        }
        return diff;
    }









}






