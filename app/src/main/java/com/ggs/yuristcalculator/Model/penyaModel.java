package com.ggs.yuristcalculator.Model;

public class penyaModel {

    String zadoljnost;
    String dnej;
    String stavka;
    String dolya;
    String peni;

    public penyaModel(String zadoljnost, String dnej, String stavka, String dolya, String peni) {
        this.zadoljnost = zadoljnost;
        this.dnej = dnej;
        this.stavka = stavka;
        this.dolya = dolya;
        this.peni = peni;
    }


    public String getZadoljnost() {
        return zadoljnost;
    }

    public String getDnej() {
        return dnej;
    }

    public String getStavka() {
        return stavka;
    }

    public String getDolya() {
        return dolya;
    }

    public String getPeni() {
        return peni;
    }
}
