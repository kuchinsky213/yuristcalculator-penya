package com.ggs.yuristcalculator;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class NeuroToast {

    public static void showToast(Activity activity){
        View view  = activity.getLayoutInflater().inflate(R.layout.layout_toast, null);


       // textToast.setText(text);
        Toast toast = new Toast(activity);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(view);
        toast.show();
}}
